@echo off

setlocal enabledelayedexpansion
set LIMIT=999

set USER=%1
set PASSWORD=%2
set PROJECT=%3
set FILTER=%4

set DESTINATION=projects\%PROJECT%

PowerShell -NoProfile -ExecutionPolicy Bypass -Command "Remove-item alias:curl"
if exist %DESTINATION% rmdir %DESTINATION% /s /q
mkdir %DESTINATION%

SET URL="https://tools.standardbank.co.za/bitbucket/rest/search/latest/projects/%PROJECT%/repos?limit=%LIMIT%&filter=%FILTER%"
echo %URL%

set /a COUNT=0
for /f %%R in ('curl --user %USER%:%PASSWORD% %URL% ^| jq --raw-output ".values[].links.clone[].href"') do (
    set REPO=%%R
    if "!REPO:~0,5!"=="https" (
        set /a COUNT+=1
        set CLONE_URL=!REPO:~0,8!%USER%:%PASSWORD%@!REPO:~8!

        for /f "tokens=6 delims=/" %%n in ("!CLONE_URL!") do set NAME=%%n
        set NAME=!NAME:~0,-4!
        echo !COUNT! cloning !NAME!

        git -c http.sslVerify=false clone !CLONE_URL! !DESTINATION!\!NAME!
    )
)
