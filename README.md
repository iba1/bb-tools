# bb-tools

## clone-project
All project's repositories will be downloaded to project/<projectname>
```
clone-project.bat <username> <password> <projectname> <filter>
```
for example:
```
clone-project.bat c12345 mypass MCAGW mca-staff-
```

## git_clone_repos.sh
```
Open in browser and save in file e.g. repos.json, e.g.: https://bitbucket.techops.usw2.upwork/rest/api/latest/repos?projectkey=SCRUM-TALENT-ACTIVATION&archived=ACTIVE&start=0&limit=1000000

run
```
./git_clone_repos.sh repos.json
```