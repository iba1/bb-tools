#!/bin/bash

FILENAME=${1}

jq -r '.size' $FILENAME | xargs -I {} echo 'size={}'
jq -r '.values[].links.clone[] | select(.name=="ssh") | .href' $FILENAME | xargs -I {} git clone '{}'